<?php

namespace Mobiledev\UserBundle\Mailer;

use Mobiledev\UserBundle\Entity\MobiledevUserInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class UserMailer
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var array
     */
    private $mobiledevUserConfig;

    /**
     * UserMailer constructor.
     * @param MailerInterface $mailer
     * @param Environment $twig
     * @param array $mobiledevUserConfig
     */
    public function __construct(MailerInterface $mailer, Environment $twig, array $mobiledevUserConfig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->mobiledevUserConfig = $mobiledevUserConfig;
    }

    /**
     * @param MobiledevUserInterface $user
     * @param string $url
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendResetting(MobiledevUserInterface $user, string $url): void
    {
        $html = $this->twig->render("@MobiledevUser/email/resetting.html.twig", ['user' => $user, 'confirmationUrl' => $url]);

        $email = (new Email())
            ->from($this->mobiledevUserConfig['from_email_address'])
            ->to($user->getEmail())
            ->subject('Réinitialisation de votre mot de passe')
            ->html($html);

        $this->mailer->send($email);
    }

    /**
     * @param MobiledevUserInterface $user
     * @param string $url
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendConfirmation(MobiledevUserInterface $user, string $url): void
    {
        $html = $this->twig->render("@MobiledevUser/email/confirmation.html.twig", ['user' => $user, 'confirmationUrl' => $url]);

        $email = (new Email())
            ->from($this->mobiledevUserConfig['from_email_address'])
            ->to($user->getEmail())
            ->subject('Confirmation de votre inscription')
            ->html($html);

        $this->mailer->send($email);
    }
}
