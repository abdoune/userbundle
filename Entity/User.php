<?php

namespace Mobiledev\UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Mobiledev\UserBundle\Validator as MobiledevAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @package Mobiledev\UserBundle\Entity
 *
 * @ORM\MappedSuperclass
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
abstract class User implements MobiledevUserInterface, UserInterface
{
    /**
     * @var string
     * @ORM\Column(type="string", length=180, unique=true)
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     */
    protected $email;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $enabled = false;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $salt;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=180, unique=true, nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    protected $roles = [];

    /**
     * @MobiledevAssert\Password
     *
     * @var string|null
     */
    protected $plainPassword;

    /**
     * @see MobiledevUserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setUsername(string $username): MobiledevUserInterface
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setEmail(string $email): MobiledevUserInterface
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function isEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setEnabled(bool $enabled) : MobiledevUserInterface
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setSalt(?string $salt): MobiledevUserInterface
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setPassword(string $password): MobiledevUserInterface
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function getLastLogin(): ?\DateTime
    {
        return $this->lastLogin;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setLastLogin(?\DateTime $lastLogin): MobiledevUserInterface
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setConfirmationToken(?string $confirmationToken): MobiledevUserInterface
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function getPasswordRequestedAt(): ?\DateTime
    {
        return $this->passwordRequestedAt;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setPasswordRequestedAt(?\DateTime $passwordRequestedAt): MobiledevUserInterface
    {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setRoles(array $roles): MobiledevUserInterface
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function setPlainPassword(?string $plainPassword): MobiledevUserInterface
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    /**
     * @see MobiledevUserInterface
     */
    public function isPasswordRequestNonExpired(int $ttl): bool
    {
        return $this->getPasswordRequestedAt() instanceof \DateTime &&
            $this->getPasswordRequestedAt()->getTimestamp() + $ttl > time();
    }
}
