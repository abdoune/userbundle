<?php


namespace Mobiledev\UserBundle\Entity;


interface MobiledevUserInterface
{
    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @param string $username
     * @return MobiledevUserInterface
     */
    public function setUsername(string $username): MobiledevUserInterface;

    /**
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * @param string $email
     * @return MobiledevUserInterface
     */
    public function setEmail(string $email): MobiledevUserInterface;

    /**
     * @return bool
     */
    public function isEnabled() : bool;

    /**
     * @param bool $enabled
     * @return MobiledevUserInterface
     */
    public function setEnabled(bool $enabled) : MobiledevUserInterface;

    /**
     * @param string|null $salt
     * @return MobiledevUserInterface
     */
    public function setSalt(?string $salt): MobiledevUserInterface;

    /**
     * @param string $password
     * @return MobiledevUserInterface
     */
    public function setPassword(string $password): MobiledevUserInterface;

    /**
     * @return \DateTime|null
     */
    public function getLastLogin(): ?\DateTime;

    /**
     * @param \DateTime|null $lastLogin
     * @return MobiledevUserInterface
     */
    public function setLastLogin(?\DateTime $lastLogin): MobiledevUserInterface;

    /**
     * @return string|null
     */
    public function getConfirmationToken(): ?string;

    /**
     * @param string|null $confirmationToken
     * @return MobiledevUserInterface
     */
    public function setConfirmationToken(?string $confirmationToken): MobiledevUserInterface;

    /**
     * @return \DateTime|null
     */
    public function getPasswordRequestedAt(): ?\DateTime;

    /**
     * @param \DateTime|null $passwordRequestedAt
     * @return MobiledevUserInterface
     */
    public function setPasswordRequestedAt(?\DateTime $passwordRequestedAt): MobiledevUserInterface;

    /**
     * @param array $roles
     * @return MobiledevUserInterface
     */
    public function setRoles(array $roles): MobiledevUserInterface;

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string;

    /**
     * @param string|null $plainPassword
     * @return MobiledevUserInterface
     */
    public function setPlainPassword(?string $plainPassword): MobiledevUserInterface;

    /**
     * @param int $ttl
     * @return bool
     */
    public function isPasswordRequestNonExpired(int $ttl): bool;
}
