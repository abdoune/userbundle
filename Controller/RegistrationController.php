<?php

namespace Mobiledev\UserBundle\Controller;

use Mobiledev\UserBundle\Entity\MobiledevUserInterface;
use Mobiledev\UserBundle\Form\RegistrationType;
use Mobiledev\UserBundle\Mailer\UserMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 * @Route("/registration")
 */
class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="mobiledev_user_registration_register")
     */
    public function registration(Request $request, TokenGeneratorInterface $tokenGenerator, array $mobiledevUserConfig): Response
    {
        $class = $mobiledevUserConfig['user_class'];
        $user = new $class();

        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setConfirmationToken($tokenGenerator->generateToken());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return new RedirectResponse($this->generateUrl('mobiledev_user_registration_send_email', array('username' => $user->getUsername())));
        }

        return $this->render('@MobiledevUser/registration/register.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/send-email/{username}", name="mobiledev_user_registration_send_email", methods={"GET"})
     *
     * @param UserMailer $mailer
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UrlGeneratorInterface $router
     * @param array $mobiledevUserConfig
     * @return Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendEmail(
        UserMailer $mailer,
        string $username,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $router,
        array $mobiledevUserConfig
    ) : Response {
        $user = $entityManager->getRepository($mobiledevUserConfig['user_class'])->findOneBy(array('username' => $username));

        if (!$user instanceof MobiledevUserInterface || $user->isEnabled()) {
            throw $this->createAccessDeniedException('Oops');
        }

        $user->setPasswordRequestedAt(new \DateTime());

        $mailer->sendConfirmation(
            $user,
            $router->generate('mobiledev_user_registration_check_email', array(
                'confirmationToken' => $user->getConfirmationToken(),
                'email'             => $user->getEmail()
            ), UrlGeneratorInterface::ABSOLUTE_URL)
        );

        return $this->render('@MobiledevUser/registration/confirmation.html.twig', [
            'email' => $user->getEmail()
        ]);
    }

    /**
     * @Route("/check-email/{confirmationToken}/{email}", name="mobiledev_user_registration_check_email", methods={"GET"})
     *
     * @param EntityManagerInterface $entityManager
     * @param array $mobiledevUserConfig
     * @param string $confirmationToken
     * @param string $email
     * @return Response
     */
    public function checkEmail(
        EntityManagerInterface $entityManager,
        array $mobiledevUserConfig,
        string $confirmationToken,
        string $email
    ) : Response {
        $user = $entityManager->getRepository($mobiledevUserConfig['user_class'])->findOneBy(array(
            'confirmationToken' => $confirmationToken,
            'email' => $email
        ));

        if (!$user instanceof MobiledevUserInterface || $user->isEnabled()) {
            throw $this->createAccessDeniedException('Oops');
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->render('@MobiledevUser/registration/confirmed.html.twig');
    }
}
