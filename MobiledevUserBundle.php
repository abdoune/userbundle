<?php

namespace Mobiledev\UserBundle;

use Mobiledev\UserBundle\DependencyInjection\MobiledevUserExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MobiledevUserBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new MobiledevUserExtension();
    }
}
